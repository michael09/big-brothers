from django.http import JsonResponse
from django.shortcuts import render
from homepage.models import TaxonomicUnits, Vernaculars

# Create your views here.
list_animal = []


def smart_form(request):
    return render(request, 'smartInput/index.html')


def get_animal_name(request):
    return JsonResponse({
        'animal': [
            {
                'value': animal.vernacular_name,
            }
            for animal in Vernaculars.objects.all()
        ]
    })


def get_animal_info(request, name):
    if request.method == 'GET':
        try:
            data = []
            ver_obj = Vernaculars.objects.filter(vernacular_name=name)
            animal_obj = ver_obj[0].tsn
            while animal_obj.rank_id.rank_name != 'Kingdom        ':
                data.append({
                    'rank_name': animal_obj.rank_id.rank_name,
                    'unit_name': animal_obj.complete_name
                })
                animal_obj = animal_obj.parent_tsn
            data.append({
                'rank_name': 'Kingdom        ',
                'unit_name': 'Animalia'
            })
            data.append({
                    'rank_name': 'Vernacular Name',
                    'unit_name': ver_obj[0].vernacular_name.upper()
                })
            return JsonResponse({
                'status': 200,
                'data': [{
                    'rank_name': i.get('rank_name'),
                    'unit_name': i.get('unit_name')
                } for i in data[::-1]]
            })
        except:
            return JsonResponse({
                'status': 404,
                'data': 'Query not match with any data'
            })
