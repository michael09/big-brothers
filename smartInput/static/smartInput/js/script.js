var animal_name = [];
//
$(document).ready(function () {
    $.ajax({
        url: '/form/get-animal-name/',
        success: function (animal) {
            console.log(animal);
            var i;
            for (i = 1; i < animal.animal.length; i++) {
                animal_name[i-1] = animal.animal[i];
            }
        }
    }).then((data) => {
        $("#animal").autocomplete({
            lookup: animal_name,
            minChars: 3,
            lookupLimit: 20,
            maxHeight: 600,
            onSelect: function (animal) {
                $('#animal').val(animal.value);
            }
        });
    });

    $('form').submit(function (e) {
        e.preventDefault();
    });
});

function getAnimalInfo() {
    $(document).ready(function () {
        const animal_name_input = $('#animal').val();
        if (animal_name_input === '') return;

        $.ajax({
            url: 'get-animal-info/' + animal_name_input + '/',
            success: function (result) {
                const url = "https://serpapi.com/playground?q=" + result.data[0].unit_name + "&tbm=isch&ijn=0";
                $.getJSON(url, function (data) {
                    console.log(url);
                });
                console.log(result);
                if ($('.result')[0]) {
                        $('.result').remove();
                    }
                if ($('.alert')[0]) {
                        $('.alert').remove()
                    }
                if (result.status === 200) {
                    html = "<div style=\"margin-top: 10px; width: 50%; display: inline-block;\" class=\"alert alert-success alert-dismissible\">\n" +
                        "                                <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\n" +
                        "                                See the <strong> <a class='alert-link' href='#result'>result</a></strong> below\n" +
                        "                            </div>";
                    $('form').append(html);
                    animal_vern_name = result.data[0].unit_name;
                    html = "<div id=\"gtco-features\" style='margin-top: 50px; text-align: center'>\n" +
                        "        <div class=\"gtco-container\">\n";
                    html = html + "<div class=\"result container\" id='result'>\n" +
                        "<h2 style=\"color: salmon; padding-top: 20px; margin-bottom: 0\">" + animal_vern_name + "</h2>\n" +
                        "                                    <div style='padding: 10px; text-align: left' >\n" +
                        "                                        <table class=\"table\">\n";
                    for (let i = 1; i < result.data.length; i++) {
                        html += "                                            <tr>\n" +
                        "                                                <td class=\"rank\">" + result.data[i].rank_name + "</td>\n" +
                        "                                                <td class=\"name\">" + result.data[i].unit_name + "</td>\n" +
                        "                                            </tr>\n";
                    }
                    html += "                                        </table>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                    "</div>"+
                        "        </div>\n" +
                        "    </div>";
                    $('#page').append(html)
                } else {

                    html = "<div style=\"margin-top: 10px; width: 50%; display: inline-block;\" class=\"alert alert-danger alert-dismissible\">\n" +
                        "                                <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\n" +
                        "                                <strong>" + animal_name_input +"</strong> does not exist. Try another name!\n" +
                        "                            </div>";
                    $('form').append(html)
                }

            }
        })
    })
}