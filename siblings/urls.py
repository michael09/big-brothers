from django.urls import path
from .views import siblings, siblings_post
app_name = "siblings"

urlpatterns = [
    path('', siblings, name='siblings'),
    path('send', siblings_post, name='sibling-post')
]