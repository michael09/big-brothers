# Welcome to our little project!
Sistem Cerdas - CSCM603130 | Fakultas Ilmu Komputer, Universitas Indonesia, Semester Ganjil 2019/2020

**Dibuat oleh: Big Brothers**

***

# Jurassic Tree
Aplikasi Jurassic Tree adalah sebuah aplikasi edukasi berbasis web yang dapat membantu pelajar untuk lebih memahami taksonomi hewan.
Jurassic Tree dapat digunakan untuk mencari kesamaan tingkatan takson antara dua hewan berbeda, mencari hewan dengan kekerabatan terdekat, dan mencari tingkatan atas dari input yang diberikan pengguna Jurassic Tree. Aplikasi edukatif ini juga bisa menampilkan hasil tree dari kesamaan tingkat takson antara dua hewan berbeda.

|                         Input                         |                         Output                         |
|-------------------------------------------------------|--------------------------------------------------------|
|`Dua buah nama spesies hewan`<br/>Contoh <i/>input</i>: **Panthera leo**(Singa) dan **Panthera tigris**(Harimau) |Takson paling dekat pada kedua hewan tersebut dan tingkatannya.<br/>Contoh <i/>output</i>: **Panthera**(Genus) dengan tree yang memetakan kedua nama spesies hewan |
|`Sebuah nama spesies hewan`<br/>Contoh <i/>input</i>: **Panthera leo**(Singa)  |Nama beberapa spesies hewan yang kekerabatannya paling dekat dengan hewan yang diinput.<br/>Contoh <i/>output</i>: **Panthera tigris**(Harimau), **Panthera pardus**(Macan tutul), dll  |
|`Sebuah nama spesies hewan`<br/>Contoh <i/>input</i>: **Panthera leo**(Singa)  |<i/>Auto generated</i> tingkatan takson jika aplikasi sudah mengenal spesies tersebut ataupun taksonominya.<br/>Contoh <i/>output</i>:<br/>**Kingdom: Animalia<br/>Filum: Chordata<br/>Kelas: Mammalia<br/>Ordo: Carnivora<br/>Famili: Felidae<br/>Genus: Panthera<br/>Spesies: P. leo**   |

## Ruang Lingkup dan Batasan Proyek
Proyek ini akan melingkup ilmu yang mempelajari penggolongan atau sistematika makhluk hidup
(taksonomi) khususnya untuk kingdom animalia. Basis data akan diambil dari website www.itis.gov,
dimana kami hanya akan mengambil basis data taksonomi tingkat kingdom animalia sampai tingkat
subspecies.

## Pendekatan Solusi
### Teori dan Konsep yang Digunakan
Logic-based Intelligent System berkembang dari prinsip Simple Knowledge Organization System
(SKOS), yaitu konsep mengenai pengelompokan dan standardisasi untuk mendukung penggunaan
Knowledge Organization Systems(KOS) seperti thesaurus, skema klasifikasi, daftar heading subjek,
dan taksonomi dalam framework dari Semantic Web. Desain dari Logic-based Intelligent System
bertujuan mengembangkan prinsip dan metode untuk membangun sistem cerdas untuk tugas-tugas
rumit yang dengan mudah diselesaikan oleh otak manusia namun menantang untuk mesin.

### Teknik AI yang Digunakan
Kami berencana menggunakan Django sebagai framework website kami dengan harapan kami lebih
akrab dengan bahasa pemrograman Python. Database Management System kami akan menggunakan
PostgreSQL. Teknik AI yang akan digunakan adalah Logics dengan Simple Knowledge
Organization System. Ide dari SKOS ini adalah bagian dari kerangka Web semantik, sistem yang
dimaksudkan untuk meningkatkan koneksi antara data yang ditautkan. Itu juga dibangun di atas
Resource Description Framework (RDF), satu set spesifikasi yang dirancang untuk mengelola data
dan metadata. SKOS didasarkan pada konsep, yang digambarkan sebagai "unit pemikiran." Memecah
konsep-konsep ini ke dalam sintaksis yang didefinisikan membantu mengatur sistem pengetahuan.
Sistem Organisasi Pengetahuan Sederhana menggunakan sesuatu yang disebut "skema konsep" untuk
mengatur konsep individu ini. Untuk permasalahan mencari ancestor dari dua nodes (hewan), kami
akan menggunakan Least Common Subsumer. Menurut definisi IGI Global, Least Common
Subsumer adalah “It is the most specific common ancestor of two concepts found in a given ontology.
Semantically, it represents the commonality of the pair of concepts. For example, the LCS of moose
and kangaroo in WordNet is mammal.”

## Status
![pipeline](https://gitlab.com/michael09/big-brothers/badges/master/build.svg)

## Anggota Kelompok
- Aditya Pratama (1706039490)
- Jeffrey (1706039843)
- Michael Wiryadinata Halim (1706039944)
- Muhamad Lutfi Arif (1706979360)