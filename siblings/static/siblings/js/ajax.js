$("form").submit(function() {
    event.preventDefault();
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var result= document.getElementById("animal").value;
    $.ajax({
        headers: { "X-CSRFToken": csrftoken },
        url: $("form").attr('action'),
        method: "POST",
        data: {
            animal: result
        },
        success: function (data) {
            var tree_structure = {
                chart: {
                    container: "#OrganiseChart6",
                    levelSeparation:    70,
                    siblingSeparation:  70,
                    subTeeSeparation:   70,
                    nodeAlign: "BOTTOM",
                    scrollbar: "fancy",
                    padding: 35,
                    node: { HTMLclass: "evolution-tree" },
                    connectors: {
                        type: "curve",
                        style: {
                            "stroke-width": 2,
                            "stroke-linecap": "round",
                            "stroke": "#ccc"
                        }
                    }
                }, nodeStructure: data.result.nodeStructure};
            new Treant(tree_structure);
        },
        error: function () {
            $("#error").append("Please check your input !, input must be species or your species name " +
                "is still invalid :((")
        }
    })});
