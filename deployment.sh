#!/bin/bash
pip install -r requirements.txt
python manage.py makemigrations homepage
python manage.py migrate homepage
python manage.py collectstatic --no-input