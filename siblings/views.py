from django.shortcuts import render
from homepage.models import TaxonomicUnits
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.http import HttpResponseBadRequest


# Create your views here.


def siblings(request):
    return render(request, 'siblings/siblings.html')


@require_POST
def siblings_post(request):
    animal_string = request.POST.get('animal')
    animal = None
    try:
        animal = TaxonomicUnits.objects.filter(complete_name__iexact=animal_string)[0]
        if animal.rank_id.rank_name.strip() != "Species" or animal.parent_tsn.rank_id == 0:
            raise Exception
    except Exception:
        return HttpResponseBadRequest()
    hierarchy = animal.hierarchy.hierarchy_string.strip().split("-")
    popLastElementsAndReverse(hierarchy)
    lst_sibling = TaxonomicUnits.objects.filter(parent_tsn=animal.parent_tsn).values_list("complete_name", 'tsn')[:10]
    res = makeTree(hierarchy, lst_sibling)
    result = {
        'nodeStructure': res[0]
    }
    return JsonResponse({'result': result})


def makeTree(hierarchy, lst_sibling):
    res = [{
        'text':
            {'name': name[0]},
        'HTMLclass': 'the-parent',
        'stackChildren': True
        ,
        'children': [{'text': {'name': name_child}} for name_child in TaxonomicUnits.objects
            .filter(parent_tsn=name[1]).values_list("complete_name", flat=True)]
    } for name in lst_sibling]

    for i in hierarchy:
        taxon = TaxonomicUnits.objects.get(tsn=i)
        tmp = [{
            'text': {'name': taxon.complete_name, 'desc': taxon.rank_id.rank_name},
            'HTMLclass': 'the-parent',
            'children': res
        }]
        res = tmp
    return res


def popLastElementsAndReverse(hierarchy):
    length = len(hierarchy)
    hierarchy.pop(length - 1)
    hierarchy.reverse()
