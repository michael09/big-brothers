from django.apps import AppConfig


class LowestparentConfig(AppConfig):
    name = 'lowestParent'
