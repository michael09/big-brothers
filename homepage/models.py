# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class TaxonUnitTypes(models.Model):
    rank_id = models.AutoField(primary_key=True)
    rank_name = models.CharField(max_length=15, blank=True, null=True)
    update_date = models.DateField(blank=True, null=True)
    dir_parent_rank_id = models.ForeignKey('TaxonUnitTypes', on_delete=models.SET_DEFAULT, default=10)

    class Meta:
        db_table = 'taxon_unit_types'


class TaxonomicUnits(models.Model):
    tsn = models.AutoField(primary_key=True)
    unit_ind1 = models.CharField(max_length=1, blank=True, null=True)
    unit_name1 = models.CharField(max_length=35, blank=True, null=True)
    unit_ind2 = models.CharField(max_length=1, blank=True, null=True)
    unit_name2 = models.CharField(max_length=35, blank=True, null=True)
    unit_ind3 = models.CharField(max_length=7, blank=True, null=True)
    unit_name3 = models.CharField(max_length=35, blank=True, null=True)
    unit_ind4 = models.CharField(max_length=7, blank=True, null=True)
    unit_name4 = models.CharField(max_length=35, blank=True, null=True)
    unnamed_taxon_ind = models.CharField(max_length=1, blank=True, null=True)
    name_usage = models.CharField(max_length=12, blank=True, null=True)
    unaccept_reason = models.CharField(max_length=50, blank=True, null=True)
    credibility_rtng = models.CharField(max_length=40, blank=True, null=True)
    completeness_rtng = models.CharField(max_length=10, blank=True, null=True)
    currency_rating = models.CharField(max_length=7, blank=True, null=True)
    phylo_sort_seq = models.SmallIntegerField(blank=True, null=True)
    initial_time_stamp = models.DateTimeField(blank=True, null=True)
    update_date = models.DateField(blank=True, null=True)
    uncertain_prnt_ind = models.CharField(max_length=3, blank=True, null=True)
    n_usage = models.TextField(blank=True, null=True)
    complete_name = models.CharField(max_length=255, blank=True, null=True)
    parent_tsn = models.ForeignKey('TaxonomicUnits', on_delete=models.SET_DEFAULT, default=0)
    rank_id = models.ForeignKey(TaxonUnitTypes, on_delete=models.CASCADE, blank=False, null=False, default=10)

    class Meta:
        db_table = 'taxonomic_units'


class Hierarchy(models.Model):
    tsn = models.OneToOneField(TaxonomicUnits, primary_key=True, on_delete=models.CASCADE)
    hierarchy_string = models.CharField(max_length=300, blank=True, null=True)
    level = models.IntegerField(blank=True, null=True)
    childrencount = models.IntegerField(blank=True, null=True)
    parent_tsn = models.ForeignKey('Hierarchy', on_delete=models.SET_DEFAULT, default=137)

    class Meta:
        db_table = 'hierarchy'


class Vernaculars(models.Model):
    tsn = models.ForeignKey(TaxonomicUnits, on_delete=models.CASCADE, default=202423)
    vernacular_name = models.CharField(max_length=80, blank=True, null=True)
    update_date = models.DateField(blank=True, null=True)
    vern_id = models.AutoField(primary_key=True)

    class Meta:
        db_table = 'vernaculars'


class Longnames(models.Model):
    tsn = models.OneToOneField(TaxonomicUnits, primary_key=True, on_delete=models.CASCADE, default=202423)
    completename = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        db_table = 'longnames'