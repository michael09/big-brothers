from django.urls import path
from .views import smart_form, get_animal_name, get_animal_info
app_name = "smart_form"

urlpatterns = [
    path('', smart_form, name='smart_form'),
    path('get-animal-name/', get_animal_name, name='get_animal_name'),
    path('get-animal-info/<str:name>/', get_animal_info, name='get_animal_info')
]
