from django.shortcuts import render
from homepage.models import TaxonomicUnits, Hierarchy, TaxonUnitTypes
from django.http import JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_GET, require_POST
from django.core import serializers


def lowestParent(request):
    return render(request, 'lowestParent/lowestParent.html')


@require_POST
def lowestParent_post(request):
    animal1_string = request.POST.get('animal1')
    animal2_string = request.POST.get('animal2')
    try:
        animal1 = TaxonomicUnits.objects.filter(complete_name__iexact=animal1_string)[0]
        animal2 = TaxonomicUnits.objects.filter(complete_name__iexact=animal2_string)[0]
        if (animal1.rank_id.rank_name.strip() != "Species" or animal1.parent_tsn.rank_id == 0) or (animal2.rank_id.rank_name.strip() != "Species" or animal2.parent_tsn.rank_id == 0):
            raise Exception
    except Exception:
        return HttpResponseBadRequest()
    hierarchy1 = animal1.hierarchy.hierarchy_string.strip().split("-")
    hierarchy2 = animal2.hierarchy.hierarchy_string.strip().split("-")
    hierarchy1.reverse()
    hierarchy2.reverse()
    res1 = None
    res2 = None
    pointer1 = 0
    pointer2 = 0
    while hierarchy1[pointer1] != hierarchy2[pointer2]:
        rank1 = TaxonomicUnits.objects.get(tsn=hierarchy1[pointer1]).rank_id.rank_id
        rank2 = TaxonomicUnits.objects.get(tsn=hierarchy2[pointer2]).rank_id.rank_id

        while rank1 != rank2:
            if rank1 > rank2:
                taxon1 = TaxonomicUnits.objects.get(tsn=hierarchy1[pointer1])
                tmp1 = [{
                    'text': {'name': taxon1.complete_name, 'desc': taxon1.rank_id.rank_name},
                    'HTMLclass': 'the-parent',
                    'children': res1
                }]
                res1 = tmp1

                tmp2 = [{'pseudo': True, 'HTMLclass': 'the-parent', 'children': res2}]
                res2 = tmp2
                pointer1 = pointer1 + 1
            else:
                taxon2 = TaxonomicUnits.objects.get(tsn=hierarchy2[pointer2])
                tmp2 = [{
                    'text': {'name': taxon2.complete_name, 'desc': taxon2.rank_id.rank_name},
                    'HTMLclass': 'the-parent',
                    'children': res2
                }]
                res2 = tmp2

                tmp1 = [{'pseudo': True, 'HTMLclass': 'the-parent', 'children': res1}]
                res1 = tmp1
                pointer2 = pointer2 + 1
            rank1 = TaxonomicUnits.objects.get(tsn=hierarchy1[pointer1]).rank_id.rank_id
            rank2 = TaxonomicUnits.objects.get(tsn=hierarchy2[pointer2]).rank_id.rank_id

        taxon1 = TaxonomicUnits.objects.get(tsn=hierarchy1[pointer1])
        taxon2 = TaxonomicUnits.objects.get(tsn=hierarchy2[pointer2])
        pointer1 = pointer1 + 1
        pointer2 = pointer2 + 1
        tmp1 = [{
            'text': {'name': taxon1.complete_name, 'desc': taxon1.rank_id.rank_name},
            'HTMLclass': 'the-parent',
            'children': res1
        }]
        res1 = tmp1

        tmp2 = [{
            'text': {'name': taxon2.complete_name, 'desc': taxon2.rank_id.rank_name},
            'HTMLclass': 'the-parent',
            'children': res2
        }]
        res2 = tmp2

    child = [res1[0], res2[0]]

    hierarchy = Hierarchy.objects.get(tsn=hierarchy1[pointer1]).hierarchy_string.strip().split("-")
    hierarchy.reverse()
    for i in hierarchy:
        taxon = TaxonomicUnits.objects.get(tsn=i)
        tmp = [{
            'text': {'name': taxon.complete_name, 'desc': taxon.rank_id.rank_name},
            'HTMLclass': 'the-parent',
            'children': child
        }]
        child = tmp

    result = {
        'nodeStructure': child[0]
    }
    return JsonResponse({'result': result})
            

