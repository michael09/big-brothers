from django.urls import path
from .views import lowestParent, lowestParent_post
app_name = "parent"

urlpatterns = [
    path('', lowestParent, name='lowest-parent'),
    path('get', lowestParent_post, name='lowest-parent-post')
]